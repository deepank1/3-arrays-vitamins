const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];




    // 1. Get all items that are available 

    function allAvailableItems(items) {
        
        return items.filter(element => {
            return element.available===true
        });
    }

    console.log(allAvailableItems(items));


    
    // 2. Get all items containing only Vitamin C.

    function onlyVitaminC_items(items,vitamin) {
        return items.filter(element=>{
            return element.contains === vitamin;
        })
    }

    console.log(onlyVitaminC_items(items, "Vitamin C"));



    // 3. Get all items containing Vitamin A.


    function vitaminA_items(items, vitamin) {
        return items.filter(element=>{
            return element.contains.includes(vitamin)
        })
    }

    console.log(vitaminA_items(items,"Vitamin A"));


    // 4. Group items based on the Vitamins that they contain in the following format:
    //     {
    //         "Vitamin C": ["Orange", "Mango"],
    //         "Vitamin K": ["Mango"],
    //     }
    //     and so on for all items and all Vitamins.


    function vitaminItemObject(items) {
        let result = {};
        items.forEach(element => {
            let temp = element.contains.split(",");

            temp.forEach(ele=>{
                if(!result[ele.trim()]){
                    result[ele.trim()] = []
                }
                result[ele.trim()].push(element.name);
            })
            
        });
        return result;
    }
        
    console.log(vitaminItemObject(items));


    
    // 5. Sort items based on number of Vitamins they contain.

    function sortItems(items){
        
        return items.sort((a,b)=>{
            
            if(a.contains.split(",").length > b.contains.split(",").length) return 1
            else if(a.contains.split(",").length < b.contains.split(",").length) return -1
            else return 0;
        })
        
    }
    console.log(sortItems(items));